<?php
    session_start();
    require_once 'config/db.php';

    if (isset($_POST['signup'])) {
        $firstnamelastname = $_POST['firstnamelastname'];
        $username = $_POST['username'];
        $password = $_POST['password'];
        $phonenumber = $_POST['phonenumber'];
        $urole = 'user';

        if (empty($firstnamelastname)){
            $_SESSION['error'] = 'กรุณากรอก ชื่อ-นามสกุล';
            header("location: index.php");
        }else if (empty($username)) {
            $_SESSION['error'] = 'กรุณากรอก username';
            header("location: index.php");
        }else if (empty($password)) {
            $_SESSION['error'] = 'กรุณากรอก password';
            header("location: index.php");
        }else if (strlen($_POST['password']) > 20 || strlen($_POST['password']) < 5) {
            $_SESSION['error'] = 'กรุณากรอก password ต้องมีความยาวระหว่าง 5 ถึง 20 ตัวอักษร';
            header("location: index.php");
        }else if (empty($phonenumber)) {
            $_SESSION['error'] = 'กรุณากรอก หมายเลขโทรศัทพ์';
            header("location: index.php");
        }else{
            try{

                $check_username = $conn->prepare("SELECT username FROM users WHERE username = :username");
                $check_username->bindParam(":username", $username);
                $check_username->execute();
                $row = $check_username->fetch(PDO::FETCH_ASSOC);

                if($row['username'] == $username){
                    $_SESSION['warning'] = 'มี Username นี้อยู่ในระบบแล้ว';
                    header("location: index.php");
                }else if (!isset($_SESSION['error'])) {
                    $passwordHash = password_hash($password, PASSWORD_DEFAULT);
                    $stmt = $conn->prepare("INSERT INTO users(firstnamelastname, username, password, phonenumber, urole) 
                                            VALUES(:firstnamelastname, :username, :password, :phonenumber, :urole)");
                    $stmt->bindParam(":firstnamelastname", $firstnamelastname);
                    $stmt->bindParam(":username", $username);
                    $stmt->bindParam(":password", $passwordHash);
                    $stmt->bindParam(":phonenumber", $phonenumber);
                    $stmt->bindParam(":urole", $urole);
                    $stmt->execute();
                    $_SESSION['success'] = "สมัครสมาชิกเรียบร้อยแล้ว! <a href='signin.php' class='alert-link'>คลิ๊กที่นี่</a> เพื่อเข้าสู่ระบบ";
                    header("location: index.php");
                }else{
                    $_SESSION['error'] = "มีบางอย่างผิดพลาด";
                    header("location: index.php");
                }

            }catch(PDOException $e) {
                echo $e->getMessage();
            }
        }
    };
?>