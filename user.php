<?php
session_start();
if (!isset($_SESSION['user_login'])) {
    $_SESSION['error'] = 'กรุณาเข้าสู่ระบบ!';
    header('location: signin.php');
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>E-Book</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
</head>

<body>

    <nav class="navbar navbar-expand-lg" style="background-color: #e3f2fd;">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">ร้านขายหนังสือออนไลน์ eBook_Shop</a>
            <form class="d-flex" role="search">
                <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success" type="submit">Search</button>
            </form>
            <div class="dropdown">
                <button class="dropbtn">User</button>
                <div class="dropdown-content">
                    <a href="#">Log out</a>
                </div>
            </div>
        </div>
    </nav>
    <div class="sidebar">
        <button class="accordion">หมวดหนังสือ</button>
        <div class="panel">
            <a href="#about">นิทาน</a>
            <a href="#about">นิยาย</a>
            <a href="#about">การ์ตูน</a>
            <a href="#about">เรื่องสั้น</a>
            <a href="#about">วรรณกรรม</a>
        </div>
        <a href="#about">รายการสั่งซื้อ</a>
        <a href="#about">ประวัติการสั่งซื้อ</a>
        <a href="#about">ชำระสินค้า</a>
        <a href="#about">คลังหนังสือ</a>
    </div>

    <div class="content">
        <h2>Responsive Sidebar Example</h2>
        <p>This example use media queries to transform the sidebar to a top navigation bar when the screen size
            is 700px or less.</p>
        <p>We have also added a media query for screens that are 400px or less, which will vertically stack and
            center the navigation links.</p>
        <h3>Resize the browser window to see the effect.</h3>
    </div>

    <script>
        var acc = document.getElementsByClassName("accordion");
        var i;

        for (i = 0; i < acc.length; i++) {
            acc[i].addEventListener("click", function() {
                this.classList.toggle("active");
                var panel = this.nextElementSibling;
                if (panel.style.display === "block") {
                    panel.style.display = "none";
                } else {
                    panel.style.display = "block";
                }
            });
        }
    </script>

</body>

</html>